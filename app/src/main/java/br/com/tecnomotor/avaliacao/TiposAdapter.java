package br.com.tecnomotor.avaliacao;

/**
 * Created by antonio on 07/08/17.
 */

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;

import android.net.Uri;
import android.os.Build;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.widget.ImageButton;

import android.widget.TextView;


import androidx.annotation.RequiresApi;
import androidx.cursoradapter.widget.CursorAdapter;

import br.com.tecnomotor.avaliacao.data.RequestContract.TiposEntry;

import static android.app.Activity.RESULT_OK;


//import android.icu.text.NumberFormat;


/**
 * {@link TiposAdapter} exposes a list of  tipos
 * from a {@link Cursor} to a {@link android.widget.ListView}.
 */
public class TiposAdapter extends CursorAdapter {



    private static Context ctx;

    public TiposAdapter(Context context, Cursor c, int flags) {
        super(context, c, flags);
        ctx=context;

    }

    /**
     * Cache of the children views for a  list item.
     */
    public static class ViewHolder {
        public final TextView itemNomeView;

        public final ImageButton btnPublicar;

        public ViewHolder(final View view) {


            itemNomeView = (TextView) view.findViewById(R.id.tipo);

            btnPublicar = (ImageButton) view.findViewById((R.id.publicar));




        }


    }


    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        // Choose the layout type

        int listaLayoutId = -1;

        listaLayoutId = R.layout.list_item_tipo;

        View view = LayoutInflater.from(context).inflate(listaLayoutId, parent, false);

        ViewHolder viewHolder = new ViewHolder(view);
        view.setTag(viewHolder);

        return view;
    }


    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void bindView(View view, Context context, Cursor cursor) {

        ViewHolder viewHolder = (ViewHolder) view.getTag();

        int viewType = getItemViewType(cursor.getPosition());

        int position = cursor.getPosition();

        String nome = cursor.getString(1);



        viewHolder.itemNomeView.setText(nome);
        viewHolder.btnPublicar.setTag(position);

    }




}

