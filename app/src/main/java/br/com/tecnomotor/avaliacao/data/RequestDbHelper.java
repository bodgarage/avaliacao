package br.com.tecnomotor.avaliacao.data;

/**
 * Created by antonio on 07/08/17.
 */
/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import android.content.Context;


import io.requery.android.database.sqlite.SQLiteDatabase;
import io.requery.android.database.sqlite.SQLiteOpenHelper;

import br.com.tecnomotor.avaliacao.data.RequestContract.TiposEntry;
import br.com.tecnomotor.avaliacao.data.RequestContract.MontadorasEntry;
import br.com.tecnomotor.avaliacao.data.RequestContract.AplicacaoEntry;

/**
 * Manages a local database for weather data.
 */
public class RequestDbHelper extends SQLiteOpenHelper {

    // If you change the database schema, you must increment the database version.
    private static final int DATABASE_VERSION = 1;

    static final String DATABASE_NAME = "rasther.db";

    public RequestDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);

    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        // Create a table to hold locations.  A location consists of the string supplied in the
        // location setting, the city name, and the latitude and longitude
        final String SQL_CREATE_TIPOS_TABLE = "CREATE TABLE " + TiposEntry.TABLE_NAME + " (" +
                TiposEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                TiposEntry.COLUMN_TIPO + " TEXT); ";

        final String SQL_CREATE_MONTADORAS_TABLE = "CREATE TABLE " + AplicacaoEntry.TABLE_NAME + " (" +
                AplicacaoEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                AplicacaoEntry.COLUMN_ID + " INTEGER NOT NULL, " +
                AplicacaoEntry.COLUMN_MOD_ID + " TEXT , " +

                AplicacaoEntry.COLUMN_MOD_NOME + " TEXT , " +
                AplicacaoEntry.COLUMN_MOD_PROGRAMACAO + " TEXT , " +
                AplicacaoEntry.COLUMN_MOD_AJUSTE + " TEXT , " +
                AplicacaoEntry.COLUMN_MOD_IDENTIFICACAO + " TEXT , " +
                AplicacaoEntry.COLUMN_MOD_DEFEITOS + " TEXT , " +
                AplicacaoEntry.COLUMN_MOD_APAGAMEMORIA + " TEXT , " +
                AplicacaoEntry.COLUMN_MOD_LEITURAS + " TEXT , " +
                AplicacaoEntry.COLUMN_MOD_ANALISEGRAFICA + " TEXT , " +
                AplicacaoEntry.COLUMN_MOD_DHCHANGE + " TEXT , " +
                AplicacaoEntry.COLUMN_MON_ID + " TEXT , " +
                AplicacaoEntry.COLUMN_MON_NOME + " TEXT , " +
                AplicacaoEntry.COLUMN_MON_TIPO + " TEXT , " +
                AplicacaoEntry.COLUMN_SIS_ID + " TEXT , " +
                AplicacaoEntry.COLUMN_SIS_NOME + " TEXT , " +
                AplicacaoEntry.COLUMN_TIP_ID + " TEXT , " +
                AplicacaoEntry.COLUMN_TIP_NOME + " TEXT , " +
                AplicacaoEntry.COLUMN_TIP_NOMESPA + " TEXT , " +
                AplicacaoEntry.COLUMN_TIPO_NOMEENG + " TEXT , " +
                AplicacaoEntry.COLUMN_VEI_ID + " TEXT , " +
                AplicacaoEntry.COLUMN_VEI_NOME + " TEXT , " +


                AplicacaoEntry.COLUMN_MOT_ID + " TEXT , " +
                AplicacaoEntry.COLUMN_MOT_NOME + " TEXT , " +
                AplicacaoEntry.COLUMN_VEICULOMOTORIZACAO + " TEXT , " +
                AplicacaoEntry.COLUMN_ANOINICIAL + " TEXT , " +
                AplicacaoEntry.COLUMN_ANOFINAL + " TEXT , " +
                AplicacaoEntry.COLUMN_POSCONECTOR + " TEXT , " +
                AplicacaoEntry.COLUMN_MODULOBUSCA + " TEXT , " +
                AplicacaoEntry.COLUMN_MODULOVIRTUAL + " TEXT , " +

                AplicacaoEntry.COLUMN_APL_CON_ID + " TEXT , " +
                AplicacaoEntry.COLUMN_APL_CON_NOME + " TEXT , " +
                AplicacaoEntry.COLUMN_APL_CONECTORPINOX + " TEXT , " +
                AplicacaoEntry.COLUMN_APL_CONECTORPINOY + " TEXT , " +
                AplicacaoEntry.COLUMN_RASTHERPC + " TEXT , " +
                AplicacaoEntry.COLUMN_MANUALTEC + " TEXT , " +


                AplicacaoEntry.COLUMN_VERSAOINICIAL + " TEXT); " ;

        final String SQL_CREATE_APLICACAO_TABLE = "CREATE TABLE " + MontadorasEntry.TABLE_NAME + " (" +
                MontadorasEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                MontadorasEntry.COLUMN_ID + " INTEGER NOT NULL, "+
                MontadorasEntry.COLUMN_NOME + " TEXT , " +
                MontadorasEntry.COLUMN_TIPO + " TEXT ); ";



        sqLiteDatabase.execSQL(SQL_CREATE_TIPOS_TABLE);
        sqLiteDatabase.execSQL(SQL_CREATE_MONTADORAS_TABLE);
        sqLiteDatabase.execSQL(SQL_CREATE_APLICACAO_TABLE);


    }


    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        // This database is only a cache for online data, so its upgrade policy is
        // to simply to discard the data and start over
        // Note that this only fires if you change the version number for your database.
        // It does NOT depend on the version number for your application.
        // If you want to update the schema without wiping data, commenting out the next 2 lines
        // should be your top priority before modifying this method.
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TiposEntry.TABLE_NAME);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + MontadorasEntry.TABLE_NAME);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + AplicacaoEntry.TABLE_NAME);


        onCreate(sqLiteDatabase);
    }



}