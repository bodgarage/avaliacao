package br.com.tecnomotor.avaliacao.data;

/**
 * Created by antonio on 02/08/17.
 */


import android.content.ContentResolver;
import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;
import android.text.format.Time;

/**
 * Defines table and column names for the weather database.
 */
public class RequestContract {

    // The "Content authority" is a name for the entire content provider, similar to the
    // relationship between a domain name and its website.  A convenient string to use for the
    // content authority is the package name for the app, which is guaranteed to be unique on the
    // device.
    public static final String CONTENT_AUTHORITY = "br.com.tecnomotor.avaliacao";

    // Use CONTENT_AUTHORITY to create the base of all URI's which apps will use to contact
    // the content provider.
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);

    // Possible paths (appended to base content URI for possible URI's)
    // For instance, content://com.example.android.sunshine.app/weather/ is a valid path for
    // looking at weather data. content://com.example.android.sunshine.app/givemeroot/ will fail,
    // as the ContentProvider hasn't been given any information on what to do with "givemeroot".
    // At least, let's hope not.  Don't be that dev, reader.  Don't be that dev.

    public static final String PATH_TIPOS = "tipos";
    public static final String PATH_MONTADORAS = "montadoras";
    public static final String PATH_APLICACAO = "aplicacao";
    // To make it easy to query for the exact date, we normalize all dates that go into
    // the database to the start of the the Julian day at UTC.



    /* Inner class that defines the table contents of the location table */
    public static final class TiposEntry implements BaseColumns {

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_TIPOS).build();

        public static final String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_TIPOS;

        // Table name
        public static final String TABLE_NAME = "tipos";

        // Column with the foreign key into the location table.
        public static final String COLUMN_TIPO = "tipo";

        public static Uri buildTipoUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }

        public static Uri buildAllTiposUri() {
            return CONTENT_URI;
        }


    }


    /* Inner class that defines the table contents of the location table */
    public static final class AplicacaoEntry implements BaseColumns {

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_APLICACAO).build();

        public static final String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_APLICACAO;

        // Table name
        public static final String TABLE_NAME = "aplicacao";

        // Column with the foreign key into the location table.
        public static final String COLUMN_ID = "aplicacao_id";
        // Human readable location string, provided by the API.  Because for styling,
        // "Mountain View" is more recognizable than 94043.
        public static final String COLUMN_MOD_ID = "mod_id";
        public static final String COLUMN_MOD_NOME = "mod_nome";
        public static final String COLUMN_MOD_PROGRAMACAO = "mod_programacao";
        public static final String COLUMN_MOD_AJUSTE = "mod_ajuste";
        public static final String COLUMN_MOD_IDENTIFICACAO = "mod_identificacao";
        public static final String COLUMN_MOD_DEFEITOS = "mod_defeitos";
        public static final String COLUMN_MOD_APAGAMEMORIA = "mod_apagamemoria";
        public static final String COLUMN_MOD_LEITURAS = "mod_leituras";
        public static final String COLUMN_MOD_ANALISEGRAFICA = "mod_analisegrafica";
        public static final String COLUMN_MOD_DHCHANGE = "mod_dhchange";

        public static final String COLUMN_MON_ID = "mon_id";
        public static final String COLUMN_MON_NOME = "mon_nome";
        public static final String COLUMN_MON_TIPO = "mon_tipo";

        public static final String COLUMN_SIS_ID = "sis_id";
        public static final String COLUMN_SIS_NOME = "sis_nome";

        public static final String COLUMN_TIP_ID = "tip_id";
        public static final String COLUMN_TIP_NOME = "tip_nome";
        public static final String COLUMN_TIP_NOMESPA = "tip_nomespa";
        public static final String COLUMN_TIPO_NOMEENG = "tip_nomeeng";

        public static final String COLUMN_VEI_ID = "vei_id";
        public static final String COLUMN_VEI_NOME = "vei_nome";

        public static final String COLUMN_MOT_ID = "mot_id";
        public static final String COLUMN_MOT_NOME = "mot_nome";

        public static final String COLUMN_VEICULOMOTORIZACAO = "veiculomotorizacao";
        public static final String COLUMN_ANOINICIAL = "anoinicial";
        public static final String COLUMN_ANOFINAL = "anofinal";

        public static final String COLUMN_POSCONECTOR = "posconector";
        public static final String COLUMN_MODULOBUSCA = "modulobusca";
        public static final String COLUMN_MODULOVIRTUAL = "modulovirtual";

        public static final String COLUMN_APL_CON_ID = "apl_con_id";
        public static final String COLUMN_APL_CON_NOME = "apl_con_nome";
        public static final String COLUMN_APL_CONECTORPINOX = "apl_conectorpinox";
        public static final String COLUMN_APL_CONECTORPINOY = "apl_conectorpinoy";
        public static final String COLUMN_RASTHERPC = "rastherpc";
        public static final String COLUMN_MANUALTEC = "manualtec";
        public static final String COLUMN_VERSAOINICIAL = "versaoinicial";

        public static Uri buildAllAplicacaoUri() {
            return CONTENT_URI;

        }

        public static Uri buildAplicacaoUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }




    }




    public static final class MontadorasEntry implements BaseColumns {

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_MONTADORAS).build();

        public static final String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_MONTADORAS;
        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_MONTADORAS;

        // Table name
        public static final String TABLE_NAME = "montadoras";

        // Column with the foreign key into the location table.
        public static final String COLUMN_ID = "montadora_id";
        // Human readable location string, provided by the API.  Because for styling,
        // "Mountain View" is more recognizable than 94043.
        public static final String COLUMN_NOME = "nome";

        public static final String COLUMN_TIPO = "tipo";


        public static Uri buildMontadorasUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }

        public static Uri buildAllMontadorasUri() {
            return CONTENT_URI;

        }

    }



}