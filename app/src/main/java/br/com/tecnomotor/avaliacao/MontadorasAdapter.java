package br.com.tecnomotor.avaliacao;

/**
 * Created by antonio on 07/08/17.
 */

import android.content.Context;
import android.database.Cursor;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.cursoradapter.widget.CursorAdapter;


//import android.icu.text.NumberFormat;


/**
 * {@link MontadorasAdapter} exposes a list of weather forecasts
 * from a {@link Cursor} to a {@link android.widget.ListView}.
 */
public class MontadorasAdapter extends CursorAdapter {



    private static Context ctx;

    public MontadorasAdapter(Context context, Cursor c, int flags) {
        super(context, c, flags);
        ctx=context;

    }

    /**
     * Cache of the children views for a forecast list item.
     */
    public static class ViewHolder {
        public final TextView itemNomeView;

        public final ImageButton btnPublicar;

        public ViewHolder(final View view) {


            itemNomeView = (TextView) view.findViewById(R.id.tipo);

            btnPublicar = (ImageButton) view.findViewById((R.id.publicar));

           /* btnPublicar.setOnClickListener(new View.OnClickListener() {

                private AlphaAnimation buttonClick = new AlphaAnimation(1F, 0.8F);

                @Override
                public void onClick(View v) {

                    v.startAnimation(buttonClick);

                    //view.findViewById(R.id.publicar).setVisibility(View.GONE);

                    int pos = Integer.parseInt(v.getTag().toString());

                    String sortOrder;

                    sortOrder = TiposEntry.TABLE_NAME +"."+TiposEntry._ID + " ASC";
                    Uri TiposUri = TiposEntry.buildAllTiposUri();

                    //System.out.println("uri 1 -"+MeusPrecosUri.toString());
                    Cursor cur1 =  ctx.getContentResolver().query(TiposUri,
                            null, null, null, sortOrder);

                    cur1.moveToPosition(pos);

                    //long id = cur1.getLong(HomeFragment.COLUMN_PRECO_GLOBAI_ID);



                    //Toast.makeText(ctx,"pos -"+pos+" --- id -"+id,500).show();
                    System.out.println("valor da posicao listview item -"+pos+cur1.getString(1));

                    Intent intent = new Intent();
                    intent.putExtra("keyName", cur1.getString(1));
                    setResult(RESULT_OK, intent);
                    finish();
                    //finalize();
                }
            });*/


        }


    }


    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        // Choose the layout type

        int listaLayoutId = -1;

        listaLayoutId = R.layout.list_item_montadoras;

        View view = LayoutInflater.from(context).inflate(listaLayoutId, parent, false);

        ViewHolder viewHolder = new ViewHolder(view);
        view.setTag(viewHolder);

        return view;
    }


    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void bindView(View view, Context context, Cursor cursor) {

        ViewHolder viewHolder = (ViewHolder) view.getTag();


        int position = cursor.getPosition();

        String nome = cursor.getString(2);



        viewHolder.itemNomeView.setText(nome);
        viewHolder.btnPublicar.setTag(position);

    }




}

