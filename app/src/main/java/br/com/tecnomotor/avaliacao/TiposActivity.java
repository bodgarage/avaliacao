package br.com.tecnomotor.avaliacao;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

import br.com.tecnomotor.avaliacao.data.RequestContract.TiposEntry;


public class TiposActivity extends AppCompatActivity {

    private AlphaAnimation buttonClick = new AlphaAnimation(1F, 0.8F);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //activity=this;
        //setTheme(R.style.AppTheme);
        setContentView(R.layout.activity_tipos);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.tipos_container, new TiposFragment())
                    .commit();
        }
    }

    // "Send text back" button click
    public void onButtonClick(View view) {


        view.startAnimation(buttonClick);

        //view.findViewById(R.id.publicar).setVisibility(View.GONE);

        int pos = Integer.parseInt(view.getTag().toString());

        String sortOrder;

        sortOrder = TiposEntry.TABLE_NAME +"."+ TiposEntry._ID + " ASC";
        Uri TiposUri = TiposEntry.buildAllTiposUri();

        //System.out.println("uri 1 -"+MeusPrecosUri.toString());
        Cursor cur1 =  getContentResolver().query(TiposUri,
                null, null, null, sortOrder);

        cur1.moveToPosition(pos);

        //long id = cur1.getLong(HomeFragment.COLUMN_PRECO_GLOBAI_ID);



        //Toast.makeText(ctx,"pos -"+pos+" --- id -"+id,500).show();
        System.out.println("valor da posicao listview item -"+pos+cur1.getString(1));

        Intent intent = new Intent();
        intent.putExtra("keyName", cur1.getString(1));
        setResult(RESULT_OK, intent);
        finish();
        // get the text from the EditText
        /*EditText editText = (EditText) findViewById(R.id.editText);
        String stringToPassBack = editText.getText().toString();

        // put the String to pass back into an Intent and close this activity
        Intent intent = new Intent();
        intent.putExtra("keyName", stringToPassBack);
        setResult(RESULT_OK, intent);
        finish();*/
    }

}