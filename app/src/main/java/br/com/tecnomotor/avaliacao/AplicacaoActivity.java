package br.com.tecnomotor.avaliacao;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AlphaAnimation;

import androidx.appcompat.app.AppCompatActivity;

import br.com.tecnomotor.avaliacao.data.RequestContract;
import br.com.tecnomotor.avaliacao.data.RequestContract.AplicacaoEntry;


public class AplicacaoActivity extends AppCompatActivity {

    private AlphaAnimation buttonClick = new AlphaAnimation(1F, 0.8F);

    private static final String sAplicacaoTipoMonId =
            AplicacaoEntry.TABLE_NAME+
                    "." + AplicacaoEntry.COLUMN_MON_TIPO + " = ?  AND "+AplicacaoEntry.COLUMN_MON_ID+ "= ? ";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //activity=this;
        //setTheme(R.style.AppTheme);
        setContentView(R.layout.activity_aplicacao);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.aplicacao_container, new AplicacaoFragment())
                    .commit();
        }
    }

    // "Send text back" button click
    public void onButtonClick(View view) {


        view.startAnimation(buttonClick);

        //view.findViewById(R.id.publicar).setVisibility(View.GONE);

        int pos = Integer.parseInt(view.getTag().toString());

        String sortOrder;

        sortOrder = AplicacaoEntry.TABLE_NAME +"."+ AplicacaoEntry._ID + " ASC";
        Uri MontadorasUri = AplicacaoEntry.buildAllAplicacaoUri();

        SharedPreferences sharedPref;
        String defaultValue, tipo,montadora_id ;
        sharedPref = getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        defaultValue = getResources().getString(R.string.type_default);
        tipo = sharedPref.getString(getString(R.string.type), defaultValue);

        montadora_id = sharedPref.getString(getString(R.string.montadora_id), defaultValue);


        String[] selectionArgs;
        String selection;


        selection = sAplicacaoTipoMonId;
        selectionArgs = new String[]{tipo, montadora_id};
        //System.out.println("uri 1 -"+MeusPrecosUri.toString());
        Cursor cur1 = getContentResolver().query(MontadorasUri,
                null, selection, selectionArgs, sortOrder);

        cur1.moveToPosition(pos);

        //long id = cur1.getLong(HomeFragment.COLUMN_PRECO_GLOBAI_ID);



        //Toast.makeText(ctx,"pos -"+pos+" --- id -"+id,500).show();
        System.out.println("valor da posicao listview item -"+pos+cur1.getString(1));

        Intent intent = new Intent();
        intent.putExtra("keyName0", cur1.getString(22));
        intent.putExtra("keyName1", cur1.getString(24));
        intent.putExtra("aplicacao_id", cur1.getString(1));
        setResult(RESULT_OK, intent);
        finish();
        // get the text from the EditText
        /*EditText editText = (EditText) findViewById(R.id.editText);
        String stringToPassBack = editText.getText().toString();

        // put the String to pass back into an Intent and close this activity
        Intent intent = new Intent();
        intent.putExtra("keyName", stringToPassBack);
        setResult(RESULT_OK, intent);
        finish();*/
    }

}