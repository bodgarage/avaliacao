package br.com.tecnomotor.avaliacao.data;

import android.annotation.TargetApi;
import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.net.Uri;

import java.util.ArrayList;



import io.requery.android.database.sqlite.SQLiteCustomExtension;
import io.requery.android.database.sqlite.SQLiteDatabase;
import io.requery.android.database.sqlite.SQLiteDatabaseConfiguration;
import io.requery.android.database.sqlite.SQLiteQueryBuilder;

import static br.com.tecnomotor.avaliacao.data.RequestContract.CONTENT_AUTHORITY;
import static br.com.tecnomotor.avaliacao.data.RequestContract.PATH_TIPOS;
import static br.com.tecnomotor.avaliacao.data.RequestContract.PATH_MONTADORAS;
import static br.com.tecnomotor.avaliacao.data.RequestContract.PATH_APLICACAO;

//import android.database.sqlite.SQLiteDatabase;
//import android.database.sqlite.SQLiteQueryBuilder;

/**
 * Created by antonio on 06/08/17.
 */

public class RequestProvider extends ContentProvider {

    // The URI Matcher used by this content provider.
    private static final UriMatcher sUriMatcher = buildUriMatcher();
    private RequestDbHelper mOpenHelper;



    static final int TIPOS = 100;
    static final int MONTADORAS = 200;
    static final int APLICACAO = 300;





    // query pra buscar os locais de um cidade_id =?

    private static final SQLiteQueryBuilder sTiposQueryBuilder;

    static{
        sTiposQueryBuilder = new SQLiteQueryBuilder();

        //This is an inner join which looks like
        //weather INNER JOIN location ON weather.location_id = location._id
        sTiposQueryBuilder.setTables(
                RequestContract.TiposEntry.TABLE_NAME );
    }

    // query pra buscar os locais de um cidade_id =?



    private static final SQLiteQueryBuilder sMontadorasQueryBuilder;


    static{
        sMontadorasQueryBuilder = new SQLiteQueryBuilder();

        //This is an inner join which looks like
        //weather INNER JOIN location ON weather.location_id = location._id
        sMontadorasQueryBuilder.setTables(
                RequestContract.MontadorasEntry.TABLE_NAME
        );


    }





    private static final SQLiteQueryBuilder sAplicacaoQueryBuilder;

    static{
        sAplicacaoQueryBuilder = new SQLiteQueryBuilder();

        //This is an inner join which looks like
        //weather INNER JOIN location ON weather.location_id = location._id
        sAplicacaoQueryBuilder.setTables(
                RequestContract.AplicacaoEntry.TABLE_NAME);
    }




    private Cursor getTipos(Uri uri, String[] projection, String sortOrder) {
        //String cidade_id = CidadesEntry.getIDFromUri(uri);

        String[] selectionArgs;
        String selection;

        //System.out.println("marca_id -"+marca_id);

        selection = null;
        selectionArgs = null;

        return sTiposQueryBuilder.query(mOpenHelper.getReadableDatabase(),
                projection,
                selection,
                selectionArgs,
                null,
                null,
                sortOrder
        );
    }


    private Cursor getMontadorasByTipo(Uri uri, String[] projection, String sortOrder) {
        //String locationSetting = LocaisEntry.getIDFromUri(uri);

        String[] selectionArgs;
        String selection;

        //System.out.println("location settings -"+locationSetting);

        selection = null;
        selectionArgs = null;

        return sMontadorasQueryBuilder.query(mOpenHelper.getReadableDatabase(),
                projection,
                selection,
                selectionArgs,
                null,
                null,
                sortOrder
        );
    }


    private Cursor getAplicacaoByID(Uri uri, String[] projection, String sortOrder) {
        //String local_id = RequestContract.PrecosGlobaisEntry.getIDFromUri2(uri);

        String[] selectionArgs;
        String selection;

        //System.out.println("local_id -"+local_id);

        selection =null;
        selectionArgs = null;

        return sAplicacaoQueryBuilder.query(mOpenHelper.getReadableDatabase(),
                projection,
                selection,
                selectionArgs,
                null,
                null,
                sortOrder
        );
    }







    /*
        Students: Here is where you need to create the UriMatcher. This UriMatcher will
        match each URI to the WEATHER, WEATHER_WITH_LOCATION, WEATHER_WITH_LOCATION_AND_DATE,
        and LOCATION integer constants defined above.  You can test this by uncommenting the
        testUriMatcher test within TestUriMatcher.
     */
    static UriMatcher buildUriMatcher() {
        // I know what you're thinking.  Why create a UriMatcher when you can use regular
        // expressions instead?  Because you're not crazy, that's why.

        // All paths added to the UriMatcher have a corresponding code to return when a match is
        // found.  The code passed into the constructor represents the code to return for the root
        // URI.  It's common to use NO_MATCH as the code for this case.
        final UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);
        final String authority = CONTENT_AUTHORITY;

        // For each type of URI you want to add, create a corresponding code.
        // usar * para qualquer tipo de dados e # para numeros

        matcher.addURI(authority, PATH_TIPOS, TIPOS);

        matcher.addURI(authority, PATH_MONTADORAS, MONTADORAS);
        matcher.addURI(authority, PATH_APLICACAO, APLICACAO);

        return matcher;
    }

    /*
        Students: We've coded this for you.  We just create a new WeatherDbHelper for later use
        here.
     */
    @Override
    public boolean onCreate() {
        mOpenHelper = new RequestDbHelper(getContext());
        return true;
    }


    /*
        Students: Here's where you'll code the getType function that uses the UriMatcher.  You can
        test this by uncommenting testGetType in TestProvider.
     */
    @Override
    public String getType(Uri uri) {

        // Use the Uri Matcher to determine what kind of URI this is.
        final int match = sUriMatcher.match(uri);

        switch (match) {
            // Student: Uncomment and fill out these two cases
            case TIPOS:
                return RequestContract.TiposEntry.CONTENT_TYPE;
            case MONTADORAS:
                return RequestContract.MontadorasEntry.CONTENT_TYPE;
            case APLICACAO:
                return RequestContract.AplicacaoEntry.CONTENT_TYPE;

            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
    }


    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs,
                        String sortOrder) {
        // Here's the switch statement that, given a URI, will determine what kind of request it is,
        // and query the database accordingly.
        Cursor retCursor;


        //System.out.println("entrei query uri - "+ uri.toString());
        //System.out.println("valor de smatcher query- "+sUriMatcher.match(uri));
        switch (sUriMatcher.match(uri)) {

            // precos validos hoja


            // "precos"
            case TIPOS: {

                //System.out.println("cursor precos");
                retCursor = sTiposQueryBuilder.query(mOpenHelper.getReadableDatabase(),
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;
            }
            case MONTADORAS: {

                //System.out.println("cursor precos");
                retCursor = mOpenHelper.getReadableDatabase().query(
                        RequestContract.MontadorasEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;
            }
            case APLICACAO: {

                //System.out.println("cursor precos");
                retCursor = mOpenHelper.getReadableDatabase().query(
                        RequestContract.AplicacaoEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;
            }


            default:

                //System.out.println(uri);
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }

        //System.out.println("saida de query");
        retCursor.setNotificationUri(getContext().getContentResolver(), uri);

        //retCursor=null; //linha para teste, retirar

        return retCursor;
    }

    /*
        Student: Add the ability to insert Locations to the implementation of this function.
     */
    @Override
    public Uri insert(Uri uri, ContentValues values) {
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        final int match = sUriMatcher.match(uri);
        Uri returnUri;



        //System.out.println("entrei insert -");
        switch (match) {

            case TIPOS: {

                long _id = db.insert(RequestContract.TiposEntry.TABLE_NAME, null, values);
                //System.out.println("id novo preco inserido"+_id);
                if ( _id > 0 )
                    returnUri = RequestContract.TiposEntry.buildTipoUri(_id);
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            }
            case MONTADORAS: {

                long _id = db.insert(RequestContract.MontadorasEntry.TABLE_NAME, null, values);
                //System.out.println("id novo local inserido"+_id);
                if ( _id > 0 )
                    returnUri = RequestContract.MontadorasEntry.buildMontadorasUri(_id);
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            }
            case APLICACAO: {
                long _id = db.insert(RequestContract.AplicacaoEntry.TABLE_NAME, null, values);
                if ( _id > 0 )
                    returnUri = RequestContract.AplicacaoEntry.buildAplicacaoUri(_id);
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            }


            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return returnUri;
    }





    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        final int match = sUriMatcher.match(uri);
        int rowsDeleted=0;
        // this makes delete all rows return the number of rows deleted

        return rowsDeleted;
    }


    @Override
    public int update(
            Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        final int match = sUriMatcher.match(uri);
        int rowsUpdated=0;


        return rowsUpdated;
    }


    @Override
    public int bulkInsert(Uri uri, ContentValues[] values) {
        int returnCount = 0;
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        final int match = sUriMatcher.match(uri);
        //System.out.println("valor de smatcher - "+sUriMatcher.match(uri));
        switch (match) {
            case TIPOS:
                db.beginTransaction();

                try {
                    for (ContentValues value : values) {

                        //System.out.println("adicionando no banco "+value.toString());
                        long _id = db.insert(RequestContract.TiposEntry.TABLE_NAME, null, value);
                        //System.out.println("id novo preco inserido"+_id);

                        if (_id != -1) {
                            returnCount++;
                        }
                    }
                    db.setTransactionSuccessful();
                } finally {
                    db.endTransaction();
                }
                getContext().getContentResolver().notifyChange(uri, null);
                return returnCount;

            case MONTADORAS:
                db.beginTransaction();

                try {
                    for (ContentValues value : values) {

                        //System.out.println("adicionando no banco "+value.toString());
                        long _id = db.insert(RequestContract.MontadorasEntry.TABLE_NAME, null, value);
                        //System.out.println("id novo local inserido"+_id);
                        if (_id != -1) {
                            returnCount++;
                        }
                    }
                    db.setTransactionSuccessful();
                } finally {
                    db.endTransaction();
                }
                getContext().getContentResolver().notifyChange(uri, null);


                return returnCount;
            case APLICACAO:
                db.beginTransaction();

                try {
                    for (ContentValues value : values) {

                        //System.out.println("adicionando no banco "+value.toString());
                        long _id = db.insert(RequestContract.AplicacaoEntry.TABLE_NAME, null, value);
                        //System.out.println("id novo tipo conteudo inserido"+_id);
                        if (_id != -1) {
                            returnCount++;
                        }
                    }
                    db.setTransactionSuccessful();
                } finally {
                    db.endTransaction();
                }
                getContext().getContentResolver().notifyChange(uri, null);


                return returnCount;

            default:
                return super.bulkInsert(uri, values);


        }
    }

    // You do not need to call this method. This is a method specifically to assist the testing
    // framework in running smoothly. You can read more at:
    // http://developer.android.com/reference/android/content/ContentProvider.html#shutdown()
    @Override
    @TargetApi(11)
    public void shutdown() {
        mOpenHelper.close();
        super.shutdown();
    }


}
