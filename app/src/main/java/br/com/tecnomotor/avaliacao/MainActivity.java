package br.com.tecnomotor.avaliacao;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.TextView;


public class MainActivity extends AppCompatActivity {

    // Add a different request code for every activity you are starting from here
    private static final int TIPOS_ACTIVITY_REQUEST_CODE = 0;
    private static final int MONTADORAS_ACTIVITY_REQUEST_CODE = 1;
    private static final int APLICACAO_ACTIVITY_REQUEST_CODE = 2;

    // dados do aplicativo

    private static Context contextOfApplication;
    private static RequestClient AppConn=null;
    private static RequestListener myListener;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        contextOfApplication = getApplicationContext();
        AppConn = new RequestClient(contextOfApplication);

        AppConn.getJSONArray(0,"");

        SharedPreferences sharedPref = getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);


        String defaultValue = getResources().getString(R.string.type_default);
        String tipo = sharedPref.getString(getString(R.string.type), defaultValue);

        String defaultValue2 = getResources().getString(R.string.montadora_id_default);
        String montadora_id = sharedPref.getString(getString(R.string.montadora_id), defaultValue2);

        String defaultValue3 = getResources().getString(R.string.aplicacao_id_default);
        String aplicacao_id = sharedPref.getString(getString(R.string.aplicacao_id), defaultValue3);

        String defaultValue4 = getResources().getString(R.string.vei_nome_default);
        String vei_nome = sharedPref.getString(getString(R.string.vei_nome), defaultValue4);

        String defaultValue5 = getResources().getString(R.string.mot_nome_default);
        String mot_nome = sharedPref.getString(getString(R.string.mot_nome), defaultValue5);

        String defaultValue6 = getResources().getString(R.string.montadora_nome_default);
        String montadora_nome = sharedPref.getString(getString(R.string.montadora_nome), defaultValue6);


        System.out.println("SharedPreferences tipo - "+tipo+String.valueOf(R.string.type));
        System.out.println("SharedPreferences montadora_id - "+montadora_id);
        System.out.println("SharedPreferences aplicacao_id - "+aplicacao_id+mot_nome+vei_nome);
        if(!tipo.equals("")){
            TextView textView = (TextView) findViewById(R.id.textView1);
            textView.setText(tipo);
            getMontadoras(tipo);
        }
        if(!montadora_id.equals("")){

            TextView textView = (TextView) findViewById(R.id.textView2);
            textView.setText(montadora_nome);
            getAplicacao(tipo, montadora_id);
        }
        if(!aplicacao_id.equals("")){
            TextView textView = (TextView) findViewById(R.id.textView3);
            textView.setText(vei_nome+""+mot_nome);
        }





    }

    // "Go to Second Activity" button click
    public void onButtonTiposClick(View view) {

        // Start the SecondActivity
        Intent intent = new Intent(this, TiposActivity.class);
        startActivityForResult(intent, TIPOS_ACTIVITY_REQUEST_CODE);
    }

    // "Go to Second Activity" button click
    public void onButtonMontadorasClick(View view) {

        // Start the SecondActivity
        Intent intent = new Intent(this, MontadorasActivity.class);
        startActivityForResult(intent, MONTADORAS_ACTIVITY_REQUEST_CODE);
    }

    // "Go to Second Activity" button click
    public void onButtonAplicacaoClick(View view) {

        // Start the SecondActivity
        Intent intent = new Intent(this, AplicacaoActivity.class);
        startActivityForResult(intent, APLICACAO_ACTIVITY_REQUEST_CODE);
    }



    private static void getTipos(){
        AppConn.getJSONArray(0,"");
    }

    private static void getMontadoras(String pmtype){
        AppConn.getJSONArray(1,"pm.type="+pmtype);
    }

    private static void getAplicacao(String pmtype, String pmassemblers){
        AppConn.getJSONObject(2,"pm.platform=1&pm.version=17&pm.type="+pmtype+"&pm.assemblers="+pmassemblers+"&pm.pageIndex=0&pm.pageSize=10");
    }

    // This method is called when the second activity finishes
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        TextView textView;
        // check that it is the XYZActivity with an OK result
        if (requestCode == TIPOS_ACTIVITY_REQUEST_CODE) {
            if (resultCode == RESULT_OK) { // Activity.RESULT_OK

                // get String data from Intent
                String returnString = data.getStringExtra("keyName");
                System.out.println("gravando shared");
                SharedPreferences sharedPref = getSharedPreferences(getString(R.string.preference_file_key),Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();
                //pega o tipo atual
                String defaultValue = getResources().getString(R.string.type_default);
                String tipo = sharedPref.getString(getString(R.string.type), defaultValue);

                // verifica se o tipo é diferente ao anterior
                // se for diferente limpa os dados da montadora e da aplicacao
                if(!returnString.equals(tipo)) {
                    editor.putString(getString(R.string.montadora_nome), "");
                    editor.putString(getString(R.string.montadora_id), "");
                    editor.putString(getString(R.string.vei_nome), "");
                    editor.putString(getString(R.string.mot_nome), "");
                    editor.putString(getString(R.string.aplicacao_id), "");
                    textView = (TextView) findViewById(R.id.textView2);
                    textView.setText("");
                    textView = (TextView) findViewById(R.id.textView3);
                    textView.setText("");

                }
                editor.putString(getString(R.string.type), returnString);
                editor.apply();
                System.out.println("gravei shared");

                getMontadoras(returnString);
                // set text view with string
                textView = (TextView) findViewById(R.id.textView1);
                textView.setText(returnString);

            }
        }else if (requestCode == MONTADORAS_ACTIVITY_REQUEST_CODE) {
            if (resultCode == RESULT_OK) { // Activity.RESULT_OK

                // get String data from Intent
                String returnString = data.getStringExtra("keyName");

                String returnString2 = data.getStringExtra("montadora_id");


                SharedPreferences sharedPref = getSharedPreferences(getString(R.string.preference_file_key),Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();

                String defaultValue2 = getResources().getString(R.string.montadora_id_default);
                String montadora_id = sharedPref.getString(getString(R.string.montadora_id), defaultValue2);
                // verifica se o tipo é diferente ao anterior
                // se for diferente limpa os dados da aplicacao
                if(!returnString2.equals(montadora_id)) {

                    editor.putString(getString(R.string.vei_nome), "");
                    editor.putString(getString(R.string.mot_nome), "");
                    editor.putString(getString(R.string.aplicacao_id), "");

                    textView = (TextView) findViewById(R.id.textView3);
                    textView.setText("");

                }

                editor.putString(getString(R.string.montadora_nome), returnString);
                editor.putString(getString(R.string.montadora_id), returnString2);

                editor.commit();

                String defaultValue = getResources().getString(R.string.type_default);
                String tipo = sharedPref.getString(getString(R.string.type), defaultValue);

                getAplicacao(tipo, returnString2);
                // set text view with string
                textView = (TextView) findViewById(R.id.textView2);
                textView.setText(returnString);
            }
        }else if (requestCode == APLICACAO_ACTIVITY_REQUEST_CODE) {
            if (resultCode == RESULT_OK) { // Activity.RESULT_OK

                // get String data from Intent
                String returnString = data.getStringExtra("keyName0");
                String returnString2 = data.getStringExtra("keyName1");
                String returnString3 = data.getStringExtra("aplicacao_id");

                SharedPreferences sharedPref = getSharedPreferences(getString(R.string.preference_file_key),Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putString(getString(R.string.vei_nome), returnString);
                editor.putString(getString(R.string.mot_nome), returnString2);
                editor.putString(getString(R.string.aplicacao_id), returnString3);
                editor.commit();
                // set text view with string
                textView = (TextView) findViewById(R.id.textView3);
                textView.setText(returnString+" "+returnString2);
            }
        }
    }

    public static Context getContext(){
        return contextOfApplication;
    }



}