package br.com.tecnomotor.avaliacao;


import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.CursorLoader;
import androidx.loader.content.Loader;

import br.com.tecnomotor.avaliacao.data.RequestContract;
import br.com.tecnomotor.avaliacao.data.RequestContract.AplicacaoEntry;

import static com.loopj.android.http.AsyncHttpClient.LOG_TAG;


/**
 * A placeholder fragment containing a simple view.
 */
public class AplicacaoFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {


    private static final int APLICACAO_LOADER = 0;

    private AplicacaoAdapter mAplicacaoAdapter;

    private static ListView listView1;


    // these constants correspond to the projection defined above, and must change if the
    // projection changes

    private static final String sAplicacaoTipoMonId =
            AplicacaoEntry.TABLE_NAME+
                    "." + AplicacaoEntry.COLUMN_MON_TIPO + " = ?  AND "+AplicacaoEntry.COLUMN_MON_ID+ "= ? ";

    // these constants correspond to the projection defined above, and must change if the
    // projection changes

    SharedPreferences sharedPref;
    String defaultValue, tipo, montadora_id ;



    public AplicacaoFragment() {

        setHasOptionsMenu(true);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_aplicacao, container, false);


        // Sort order:  Ascending, by date.
        String sortOrder;

        sortOrder = AplicacaoEntry.TABLE_NAME +"."+AplicacaoEntry._ID + " ASC";
        Uri MontadorasUri = AplicacaoEntry.buildAllAplicacaoUri();

        sharedPref = getActivity().getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        defaultValue = getResources().getString(R.string.type_default);
        tipo = sharedPref.getString(getString(R.string.type), defaultValue);

        montadora_id = sharedPref.getString(getString(R.string.montadora_id), defaultValue);


        String[] selectionArgs;
        String selection;


        selection = sAplicacaoTipoMonId;
        selectionArgs = new String[]{tipo, montadora_id};
        //System.out.println("uri 1 -"+MeusPrecosUri.toString());
        Cursor cur1 = getActivity().getContentResolver().query(MontadorasUri,
                null, selection, selectionArgs, sortOrder);
        mAplicacaoAdapter = new AplicacaoAdapter(getActivity(), null, 0);
        //System.out.println("cursor 1 -"+cur1.getColumnIndex("_id"));
        //System.out.println("passei");


        // Get a reference to the ListView, and attach this adapter to it.

        listView1 = (ListView) rootView.findViewById(R.id.listview_aplicacao);

        listView1.setAdapter(mAplicacaoAdapter);



        return rootView;
    }



    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        getLoaderManager().initLoader(APLICACAO_LOADER, null, this);

        super.onActivityCreated(savedInstanceState);
    }


    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {

        CursorLoader loader=null;
        String sortOrder="";

        switch (id) {
            case APLICACAO_LOADER:
                //System.out.println("passei no loader de precos");
                //sortOrder = "min(precoPraticado) ASC";
                Uri aplicacaoUri = AplicacaoEntry.buildAllAplicacaoUri();
                String[] selectionArgs;
                String selection;


                selection = sAplicacaoTipoMonId;
                selectionArgs = new String[]{tipo, montadora_id};
                loader= new CursorLoader(getActivity(),
                        aplicacaoUri,
                        null,
                        selection,
                        selectionArgs,
                        sortOrder);
                break;



        }
        return loader;


    }


    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        Log.v(LOG_TAG, "In onLoadFinished");

        switch (loader.getId()) {
            case APLICACAO_LOADER:
                mAplicacaoAdapter.swapCursor(data);
                break;



        }
        //HomeFragment.ListUtils.setDynamicHeight(listView1);

    }


    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        switch (loader.getId()) {
            case APLICACAO_LOADER:
                mAplicacaoAdapter.swapCursor(null);
                break;
        };

    }


}