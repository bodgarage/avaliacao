package br.com.tecnomotor.avaliacao;


import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.CursorLoader;
import androidx.loader.content.Loader;

import br.com.tecnomotor.avaliacao.data.RequestContract.TiposEntry;

import static com.loopj.android.http.AsyncHttpClient.LOG_TAG;


/**
 * A placeholder fragment containing a simple view.
 */
public class TiposFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {


    private static final int TIPOS_LOADER = 0;

    private TiposAdapter mTiposAdapter;

    private static ListView listView1;


    // these constants correspond to the projection defined above, and must change if the
    // projection changes




    public TiposFragment() {

        setHasOptionsMenu(true);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_tipos, container, false);


        // Sort order:  Ascending, by date.
        String sortOrder;

        sortOrder = TiposEntry.TABLE_NAME +"."+TiposEntry._ID + " ASC";
        Uri TiposUri = TiposEntry.buildAllTiposUri();
               ;
        //System.out.println("uri 1 -"+MeusPrecosUri.toString());
        Cursor cur1 = getActivity().getContentResolver().query(TiposUri,
                null, null, null, sortOrder);
        mTiposAdapter = new TiposAdapter(getActivity(), null, 0);
        //System.out.println("cursor 1 -"+cur1.getColumnIndex("_id"));
        //System.out.println("passei");


        // Get a reference to the ListView, and attach this adapter to it.

        listView1 = (ListView) rootView.findViewById(R.id.listview_tipos);

        listView1.setAdapter(mTiposAdapter);



        return rootView;
    }



    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        getLoaderManager().initLoader(TIPOS_LOADER, null, this);

        super.onActivityCreated(savedInstanceState);
    }


    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {

        CursorLoader loader=null;
        String sortOrder="";

        switch (id) {
            case TIPOS_LOADER:
                //System.out.println("passei no loader de precos");
                //sortOrder = "min(precoPraticado) ASC";
                Uri tiposUri = TiposEntry.buildAllTiposUri();
                loader= new CursorLoader(getActivity(),
                        tiposUri,
                        null,
                        null,
                        null,
                        sortOrder);
                break;



        }
        return loader;


    }


    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        Log.v(LOG_TAG, "In onLoadFinished");

        switch (loader.getId()) {
            case TIPOS_LOADER:
                mTiposAdapter.swapCursor(data);
                break;



        }
        //HomeFragment.ListUtils.setDynamicHeight(listView1);

    }


    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        switch (loader.getId()) {
            case TIPOS_LOADER:
                mTiposAdapter.swapCursor(null);
                break;
        };

    }


}