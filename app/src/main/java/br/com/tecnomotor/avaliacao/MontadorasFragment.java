package br.com.tecnomotor.avaliacao;


import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.CursorLoader;
import androidx.loader.content.Loader;

import br.com.tecnomotor.avaliacao.data.RequestContract.MontadorasEntry;

import static com.loopj.android.http.AsyncHttpClient.LOG_TAG;


/**
 * A placeholder fragment containing a simple view.
 */
public class MontadorasFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {


    private static final int MONTADORAS_LOADER = 0;

    private MontadorasAdapter mMontadorasAdapter;

    private static ListView listView1;


    private static final String sMontadoraTipo =
            MontadorasEntry.TABLE_NAME+
                    "." + MontadorasEntry.COLUMN_TIPO + " = ? ";

    // these constants correspond to the projection defined above, and must change if the
    // projection changes

    SharedPreferences sharedPref;
    String defaultValue, tipo ;


    public MontadorasFragment() {

        setHasOptionsMenu(true);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_montadoras, container, false);


        // Sort order:  Ascending, by date.
        String sortOrder;

        sortOrder = MontadorasEntry.TABLE_NAME +"."+MontadorasEntry._ID + " ASC";
        Uri MontadorasUri = MontadorasEntry.buildAllMontadorasUri();

        sharedPref = getActivity().getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        defaultValue = getResources().getString(R.string.type_default);
        tipo = sharedPref.getString(getString(R.string.type), defaultValue);

        String[] selectionArgs;
        String selection;


        selection = sMontadoraTipo;
        selectionArgs = new String[]{tipo};

        //System.out.println("uri 1 -"+MeusPrecosUri.toString());
        Cursor cur1 = getActivity().getContentResolver().query(MontadorasUri,
                null, selection, selectionArgs, sortOrder);
        mMontadorasAdapter = new MontadorasAdapter(getActivity(), null, 0);
        //System.out.println("cursor 1 -"+cur1.getColumnIndex("_id"));
        //System.out.println("passei");


        // Get a reference to the ListView, and attach this adapter to it.

        listView1 = (ListView) rootView.findViewById(R.id.listview_montadoras);

        listView1.setAdapter(mMontadorasAdapter);



        return rootView;
    }



    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        getLoaderManager().initLoader(MONTADORAS_LOADER, null, this);

        super.onActivityCreated(savedInstanceState);
    }


    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {

        CursorLoader loader=null;
        String sortOrder="";

        switch (id) {
            case MONTADORAS_LOADER:
                //System.out.println("passei no loader de precos");
                //sortOrder = "min(precoPraticado) ASC";



                //tipo = sharedPref.getString(getString(R.string.type), defaultValue);

                String[] selectionArgs;
                String selection;


                selection = sMontadoraTipo;
                selectionArgs = new String[]{tipo};

                Uri montadorasUri = MontadorasEntry.buildAllMontadorasUri();
                loader= new CursorLoader(getActivity(),
                        montadorasUri,
                        null,
                        selection,
                        selectionArgs,
                        sortOrder);
                break;



        }
        return loader;


    }


    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        Log.v(LOG_TAG, "In onLoadFinished");

        switch (loader.getId()) {
            case MONTADORAS_LOADER:
                mMontadorasAdapter.swapCursor(data);
                break;



        }
        //HomeFragment.ListUtils.setDynamicHeight(listView1);

    }


    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        switch (loader.getId()) {
            case MONTADORAS_LOADER:
                mMontadorasAdapter.swapCursor(null);
                break;
        };

    }


}