package br.com.tecnomotor.avaliacao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;


import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.MySSLSocketFactory;
import com.loopj.android.http.PersistentCookieStore;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;


import br.com.tecnomotor.avaliacao.data.RequestContract.AplicacaoEntry;
import br.com.tecnomotor.avaliacao.data.RequestContract.MontadorasEntry;
import br.com.tecnomotor.avaliacao.data.RequestContract.TiposEntry;

import cz.msebera.android.httpclient.Header;

/**
 * Created by antonio on 27/07/17.
 */



public class RequestClient {

    private final String LOG_TAG = RequestClient.class.getSimpleName();



    public static AsyncHttpClient Client = new AsyncHttpClient();
    public static PersistentCookieStore myCookieStore;
    private static Context contextOfApplication;

    private RequestListener mOnEventListener;



    private boolean DEBUG = true;

    public int inserted=0;
    public List<List<HashMap<String, String>>> data;
    public int deleted=0;



    public String urlAPI = "https://service.tecnomotor.com.br/iRasther";
    String urlRequest[] = {"tipo", //0
            "montadora", //1
            "aplicacao",      //2
           };


    public RequestClient( Context contextOfApplication) {

        RequestClient.contextOfApplication= MainActivity.getContext();

        myCookieStore = new PersistentCookieStore(contextOfApplication);

        Client.setCookieStore(myCookieStore);

        //System.out.println(myCookieStore.getCookies().toString());
        MySSLSocketFactory socketFactory=null;
        try {
            //            KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
            //            trustStore.load(null, null);
            socketFactory = new MySSLSocketFactory(buildKeyStore(contextOfApplication, R.raw.certificate_ca));
            socketFactory.setHostnameVerifier(MySSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Client.setTimeout(30 * 1000);
        if (socketFactory != null) {
            //            socketFactory.setHostnameVerifier(MySSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
            Client.setSSLSocketFactory(socketFactory);

        }
        Client.setMaxRetriesAndTimeout(1, 30000);
        //Client.setUserAgent("PegPreçoAPP");


        //Client.setURLEncodingEnabled(true);
        //Client.addHeader("Accept", "application/json");
        //Client.addHeader("Content-Type", "application/json");

        Client.addHeader("Accept", "application/json");
        //Client.addHeader("Content-Type", "application/x-www-form-urlencoded");
    }



    //listener do objeto para avisos de recebimento
    public void setOnEventListener(RequestListener listener) {

        System.out.println("definindo o listener");
        this.mOnEventListener = listener;
    }

    // API Tipos

    // Método que processa e armazena o Json Recebido de tipos
    private int getTiposFromJson(String jsonStr)
            throws JSONException {

        // Precos information
        final String OWM_TIPO = "tipo";


        int inserted = 0;
        int updated = 0;

        try{

            JSONArray Json = new JSONArray(jsonStr);


            Vector<ContentValues> cVVector = new Vector<ContentValues>();

            for(int i=0;i<Json.length();i++)
            {
                System.out.println(Json.getString(i));
                String tipo=Json.getString(i);


                // First, check if the location with this city name exists in the db
                Cursor locationCursor = contextOfApplication.getContentResolver().query(
                        TiposEntry.CONTENT_URI,
                        new String[]{TiposEntry.COLUMN_TIPO},
                        TiposEntry.COLUMN_TIPO + " = ?",
                        new String[]{tipo},
                        null);

                if (locationCursor.moveToFirst()) {
                    //System.out.println("ja existe preco "+id);
                    //atualiza ou apaga preço


                }else {

                    ContentValues tiposValues = new ContentValues();

                    tiposValues.put(TiposEntry.COLUMN_TIPO, tipo);

                    cVVector.add(tiposValues);

                }
            }


            // add to database
            if ( cVVector.size() > 0 ) {
                ContentValues[] cvArray = new ContentValues[cVVector.size()];
                cVVector.toArray(cvArray);
                inserted = contextOfApplication.getContentResolver().bulkInsert(TiposEntry.CONTENT_URI, cvArray);
            }


            //Log.d(LOG_TAG, "RepofyClientTask(getPrecosValidosHojeByCityFromJson) Complete. " + inserted + " Inserted");

        } catch (JSONException e) {
            Log.e(LOG_TAG, e.getMessage(), e);
            e.printStackTrace();
        }


        return inserted;
    }

    // API Montadoras
    // Método que processa e armazena o Json Recebido de Montadoras
    private int getMontadorasFromJSON(String jsonStr)
            throws JSONException {

        // Precos information
        final String OWM_TIPO = "tipo";


        int inserted = 0;
        int updated = 0;

        try{

            //System.out.println("meujson"+jsonStr);
            JSONArray Json = new JSONArray(jsonStr);
            Vector<ContentValues> cVVector = new Vector<ContentValues>();
            for(int i=0;i<Json.length();i++) {
                String id, nome, tipo;
                System.out.println(Json.getString(i));

                JSONObject register = new JSONObject(Json.getString(i));
                System.out.println(register.getInt("id"));

                id=register.getString("id");
                nome=register.getString("nome");
                tipo=register.getString("tipo");
                Cursor locationCursor = contextOfApplication.getContentResolver().query(
                        MontadorasEntry.CONTENT_URI,
                        new String[]{MontadorasEntry.COLUMN_ID},
                        MontadorasEntry.COLUMN_ID + " = ?",
                        new String[]{id},
                        null);

                if (locationCursor.moveToFirst()) {
                    //System.out.println("ja existe preco "+id);
                    //atualiza ou apaga preço


                }else {

                    ContentValues montadorasValues = new ContentValues();

                    montadorasValues.put(MontadorasEntry.COLUMN_ID, id);
                    montadorasValues.put(MontadorasEntry.COLUMN_NOME, nome);
                    montadorasValues.put(MontadorasEntry.COLUMN_TIPO, tipo);


                    cVVector.add(montadorasValues);


                }
            }






            // add to database
            if ( cVVector.size() > 0 ) {
                ContentValues[] cvArray = new ContentValues[cVVector.size()];
                cVVector.toArray(cvArray);
                inserted = contextOfApplication.getContentResolver().bulkInsert(MontadorasEntry.CONTENT_URI, cvArray);
            }


            //Log.d(LOG_TAG, "RepofyClientTask(getPrecosValidosHojeByCityFromJson) Complete. " + inserted + " Inserted");

        } catch (JSONException e) {
            Log.e(LOG_TAG, e.getMessage(), e);
            e.printStackTrace();
        }


        return inserted;
    }


    // API Aplicacao
    // Método que processa e armazena o Json Recebido de Aplicacao
    private int getAplicacaoFromJSON(String jsonStr)
            throws JSONException {

        // Precos information
        final String OWM_TIPO = "tipo";


        int inserted = 0;
        int updated = 0;

        try{
            System.out.println("meujson"+jsonStr);
            JSONObject Json = new JSONObject(jsonStr);


            Iterator iterator = Json.keys();
            Vector<ContentValues> cVVector = new Vector<ContentValues>();
            while (iterator.hasNext()) {
                String key = (String) iterator.next();
                System.out.println("minhaaplicacao"+Json.get(key));

                JSONArray retorno=Json.getJSONArray(key);

                for(int i=0;i<retorno.length();i++) {

                    JSONObject register = retorno.getJSONObject(i);

                    System.out.println(retorno.getJSONObject(i).get("id"));
                    String id, veiculomotorizacao,anoinicial,anofinal,posconector,modulobusca,modulovirtual, rastherpc,manualtec,versaoinicial;
                    String mod_id,mod_nome,mod_programacao,mod_ajuste,mod_identificacao,mod_defeitos,mod_apagamemoria,mod_leituras,mod_analisegrafica,mod_dhchange;
                    String mon_id,mon_nome,mon_tipo;
                    String sis_id, sis_nome;
                    String tip_id,tip_nome,tip_nomespa,tip_nomeeng;
                    String vei_id,vei_nome;
                    String mot_id,mot_nome;
                    String apl_con_id,apl_con_nome,apl_conectorpinox,apl_conectorpinoy;

                    id=register.getString("id");

                    veiculomotorizacao=register.getString("veiculoMotorizacao");
                    anoinicial=register.getString("anoInicial");
                    try {
                        anofinal = register.getString("anoFinal");
                    }catch (JSONException e){
                        anofinal ="";
                        System.out.println("nao tem ano final");
                    }


                    posconector=register.getString("posConector");
                    modulobusca=register.getString("moduloBusca");
                    modulovirtual=register.getString("moduloVirtual");
                    rastherpc=register.getString("rastherPc");
                    manualtec=register.getString("manualtec");
                    versaoinicial=register.getString("versaoInicial");

                    System.out.println("dados registro -"+id+veiculomotorizacao+anoinicial+anofinal+posconector+modulobusca+modulovirtual+rastherpc+manualtec+versaoinicial);

                    // Human readable location string, provided by the API.  Because for styling,
                    // "Mountain View" is more recognizable than 94043.
                    JSONObject modulo;
                    modulo = register.getJSONObject("modulos");
                    mod_id= modulo.getString("id");
                    try {
                    mod_nome= modulo.getString("nome");
                    }catch (JSONException e){
                        mod_nome ="";
                        System.out.println("nao tem ano final");
                    }
                    mod_programacao= modulo.getString("programacao");
                    mod_ajuste= modulo.getString("ajuste");
                    mod_identificacao= modulo.getString("identificacao");
                    mod_defeitos= modulo.getString("defeitos");
                    mod_apagamemoria= modulo.getString("apagamemoria");
                    mod_leituras= modulo.getString("leituras");
                    mod_analisegrafica= modulo.getString("analisegrafica");
                    mod_dhchange= modulo.getString("dhchange");


                    System.out.println("dados modulo -"+mod_id+mod_nome+mod_programacao+mod_ajuste+mod_identificacao+mod_defeitos+mod_apagamemoria+mod_leituras+mod_analisegrafica+mod_dhchange);

                    JSONObject montadora;
                    montadora = register.getJSONObject("montadora");
                    mon_id=montadora.getString("id");
                    mon_nome=montadora.getString("nome");
                    mon_tipo=montadora.getString("tipo");

                    System.out.println("dados montadora -"+mon_id+mon_nome+mon_tipo);

                    JSONObject sistema;
                    sistema = register.getJSONObject("sistema");
                    sis_id=sistema.getString("id");
                    sis_nome=sistema.getString("nome");

                    System.out.println("dados sistema -"+sis_id+sis_nome);

                    JSONObject tipoSistema;
                    tipoSistema = register.getJSONObject("tipoSistema");
                    tip_id=tipoSistema.getString("id");
                    tip_nome=tipoSistema.getString("nome");
                    tip_nomespa=tipoSistema.getString("nomeSpa");
                    tip_nomeeng=tipoSistema.getString("nomeEng");

                    System.out.println("tipo sistema -"+tip_id+tip_nome+tip_nomespa+tip_nomeeng);

                    JSONObject veiculo;
                    veiculo = register.getJSONObject("veiculo");
                    vei_id=veiculo.getString("id");
                    vei_nome=veiculo.getString("nome");

                    System.out.println("veiculo -"+vei_id+vei_nome);

                    JSONObject motorizacao;
                    motorizacao = register.getJSONObject("motorizacao");
                    mot_id=motorizacao.getString("id");
                    mot_nome=motorizacao.getString("nome");

                    System.out.println("veiculo -"+vei_id+vei_nome);

                    JSONArray aplicacaoConectorList;
                    aplicacaoConectorList = register.getJSONArray("aplicacaoConectorList");
                    JSONObject register2;
                    register2 = aplicacaoConectorList.getJSONObject(0);
                    System.out.println("conector -"+register2);
                    JSONObject conector;
                    conector=register2.getJSONObject("conector");
                    apl_con_id=conector.getString("id");
                    apl_con_nome=conector.getString("nome");
                    apl_conectorpinox=register2.getString("conectorPinoX");
                    apl_conectorpinoy=register2.getString("conectorPinoY");


                    //System.out.println("aplicacaoconectorlist -"+apl_con_id+apl_con_nome+apl_conectorpinox+apl_conectorpinoy);


                    Cursor locationCursor = contextOfApplication.getContentResolver().query(
                            AplicacaoEntry.CONTENT_URI,
                            new String[]{AplicacaoEntry.COLUMN_ID},
                            AplicacaoEntry.COLUMN_ID + " = ?",
                            new String[]{id},
                            null);

                    if (locationCursor.moveToFirst()) {
                        //System.out.println("ja existe  "+id);



                    }else {

                        ContentValues aplicacaoValues = new ContentValues();

                        aplicacaoValues.put(AplicacaoEntry.COLUMN_ID, id);



                        aplicacaoValues.put(AplicacaoEntry.COLUMN_VEICULOMOTORIZACAO,veiculomotorizacao);
                        aplicacaoValues.put(AplicacaoEntry.COLUMN_ANOINICIAL,anoinicial);

                        aplicacaoValues.put(AplicacaoEntry.COLUMN_ANOFINAL,anofinal);

                        aplicacaoValues.put(AplicacaoEntry.COLUMN_POSCONECTOR,posconector);
                        aplicacaoValues.put(AplicacaoEntry.COLUMN_MODULOBUSCA,modulobusca);
                        aplicacaoValues.put(AplicacaoEntry.COLUMN_MODULOVIRTUAL,modulovirtual);
                        aplicacaoValues.put(AplicacaoEntry.COLUMN_RASTHERPC,rastherpc);
                        aplicacaoValues.put(AplicacaoEntry.COLUMN_MANUALTEC,manualtec);
                        aplicacaoValues.put(AplicacaoEntry.COLUMN_VERSAOINICIAL,versaoinicial);

                        aplicacaoValues.put(AplicacaoEntry.COLUMN_MOD_ID,mod_id);
                        aplicacaoValues.put(AplicacaoEntry.COLUMN_MOD_NOME,mod_nome);
                        aplicacaoValues.put(AplicacaoEntry.COLUMN_MOD_PROGRAMACAO,mod_programacao);
                        aplicacaoValues.put(AplicacaoEntry.COLUMN_MOD_AJUSTE,mod_ajuste);
                        aplicacaoValues.put(AplicacaoEntry.COLUMN_MOD_IDENTIFICACAO,mod_identificacao);
                        aplicacaoValues.put(AplicacaoEntry.COLUMN_MOD_DEFEITOS,mod_defeitos);
                        aplicacaoValues.put(AplicacaoEntry.COLUMN_MOD_APAGAMEMORIA,mod_apagamemoria);
                        aplicacaoValues.put(AplicacaoEntry.COLUMN_MOD_LEITURAS,mod_leituras);
                        aplicacaoValues.put(AplicacaoEntry.COLUMN_MOD_ANALISEGRAFICA,mod_analisegrafica);
                        aplicacaoValues.put(AplicacaoEntry.COLUMN_MOD_DHCHANGE,mod_dhchange);

                        aplicacaoValues.put(AplicacaoEntry.COLUMN_MON_ID,mon_id.trim());
                        aplicacaoValues.put(AplicacaoEntry.COLUMN_MON_NOME,mon_nome);
                        aplicacaoValues.put(AplicacaoEntry.COLUMN_MON_TIPO,mon_tipo.trim());


                        aplicacaoValues.put(AplicacaoEntry.COLUMN_SIS_ID,sis_id);
                        aplicacaoValues.put(AplicacaoEntry.COLUMN_SIS_NOME,sis_nome);


                        aplicacaoValues.put(AplicacaoEntry.COLUMN_TIP_ID,tip_id);
                        aplicacaoValues.put(AplicacaoEntry.COLUMN_TIP_NOME,tip_nome);
                        aplicacaoValues.put(AplicacaoEntry.COLUMN_TIP_NOMESPA,tip_nomespa);
                        aplicacaoValues.put(AplicacaoEntry.COLUMN_TIPO_NOMEENG,tip_nomeeng);


                        aplicacaoValues.put(AplicacaoEntry.COLUMN_VEI_ID,vei_id);
                        aplicacaoValues.put(AplicacaoEntry.COLUMN_VEI_NOME,vei_nome);


                        aplicacaoValues.put(AplicacaoEntry.COLUMN_MOT_ID, mot_id);
                        aplicacaoValues.put(AplicacaoEntry.COLUMN_MOT_NOME,mot_nome);

                        cVVector.add(aplicacaoValues);


                    }
                }

                System.out.println("retorno"+retorno);
            }


            // add to database
            if ( cVVector.size() > 0 ) {
                ContentValues[] cvArray = new ContentValues[cVVector.size()];
                cVVector.toArray(cvArray);
                inserted = contextOfApplication.getContentResolver().bulkInsert(AplicacaoEntry.CONTENT_URI, cvArray);
            }


            //Log.d(LOG_TAG, "RepofyClientTask(getPrecosValidosHojeByCityFromJson) Complete. " + inserted + " Inserted");

        } catch (JSONException e) {
            Log.e(LOG_TAG, e.getMessage(), e);
            e.printStackTrace();
        }


        return inserted;
    }





    // Get data that requires authentication

    public void getJSONArray(final int urlMetodo, String parametros) {


        //Log.d("request", urlAPI+"/"+urlRequest[urlMetodo]);
        System.out.println(urlAPI + "/" +urlRequest[urlMetodo] +"?"+ parametros);


        Client.get(urlAPI + "/" + urlRequest[urlMetodo] +"?"+ parametros, new JsonHttpResponseHandler() {


            @Override
            public boolean getUseSynchronousMode() {
                return false;
            }

            @Override
            public void setUseSynchronousMode(boolean useSynchronousMode) {

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray success) {


                //String r = response.toString();
                String response = "";
                String r = success.toString();
                System.out.print("retorno r (metodo intercepta JSON) :" + r);
                if (urlMetodo == 0) {
                    try {
                        inserted = getTiposFromJson(success.toString());
                        //System.out.print("repofyclient getcidades inserted" + inserted);
                        if(inserted>0) {
                            //PegPrecoNotification x = new PegPrecoNotification();
                            //x.notificationUser(contextOfApplication, 0, inserted);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    //Log.d("Response", r);
                }else  if (urlMetodo == 1) {
                    try {
                        inserted = getMontadorasFromJSON(success.toString());
                        //System.out.print("repofyclient getcidade setting" + inserted);
                        if(inserted>0) {

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    //Log.d("Response", r);
                } else if (urlMetodo == 2) {
                    try {
                        inserted = getAplicacaoFromJSON(response.toString());
                        //System.out.print("repofyclient getlocalizacao inserted" + inserted);
                        if(inserted>0) {
                            //PegPrecoNotification x = new PegPrecoNotification();
                            //x.notificationUser(contextOfApplication, 3, inserted);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    //Log.d("Response", r);
                }


                //System.out.println("ret - " + response.toString());


            }


            public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)

                Log.d("Response", String.valueOf(statusCode));
            }
        });

        //return resposta;


    }


    // Get data that requires authentication

    public void getJSONObject(final int urlMetodo, String parametros) {


        //Log.d("request", urlAPI+"/"+urlRequest[urlMetodo]);

        System.out.println(urlAPI + "/" +urlRequest[urlMetodo]+"?"+ parametros);

        Client.get(urlAPI + "/" + urlRequest[urlMetodo] +"?"+ parametros, new JsonHttpResponseHandler() {


            @Override
            public boolean getUseSynchronousMode() {
                return false;
            }

            @Override
            public void setUseSynchronousMode(boolean useSynchronousMode) {

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {


                String r = response.toString();

                //String r = success.toString();
                System.out.print("retorno r (metodo intercepta JSONOBJECT) :" + r);
                if (urlMetodo == 1) {
                    try {
                        inserted = getMontadorasFromJSON(response.toString());
                        //System.out.print("repofyclient getcidade setting" + inserted);
                        if(inserted>0) {

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    //Log.d("Response", r);
                } else if (urlMetodo == 2) {
                    try {
                        inserted = getAplicacaoFromJSON(response.toString());
                        //System.out.print("repofyclient getlocalizacao inserted" + inserted);
                        if(inserted>0) {
                            //PegPrecoNotification x = new PegPrecoNotification();
                            //x.notificationUser(contextOfApplication, 3, inserted);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    //Log.d("Response", r);
                }


                //System.out.println("ret - " + response.toString());


            }


            public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)

                Log.d("Response", String.valueOf(statusCode));
            }
        });

        //return resposta;


    }



    //Funçoes para verificar e armazenar certificado SSL

    private static KeyStore buildKeyStore(Context context, int certRawResId) {
        // init a default key store
        String keyStoreType = KeyStore.getDefaultType();
        KeyStore keyStore = null;
        try {
            keyStore = KeyStore.getInstance(keyStoreType);
            keyStore.load(null, null);
            // read and add certificate authority
            Certificate cert = readCert(context, certRawResId);
            keyStore.setCertificateEntry("ca", cert);
        } catch (KeyStoreException e) {
            e.printStackTrace();
        } catch (CertificateException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return keyStore;
    }


    private static Certificate readCert(Context context, int certResourceId)  {
        // read certificate resource
        InputStream caInput = context.getResources().openRawResource(certResourceId);

        Certificate ca=null;
        try {
            // generate a certificate
            CertificateFactory cf = CertificateFactory.getInstance("X.509","BC");
            ca = cf.generateCertificate(caInput);
        } catch (CertificateException e) {
            e.printStackTrace();
        } catch (NoSuchProviderException e) {
            e.printStackTrace();
        } finally {
            try {
                caInput.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return ca;
    }



}
